CC=gcc
CFLAGS=-I.

build: build/minheap.o build/priority_queue.o build/main.o | outDir
	$(CC) -o out/c_containers build/minheap.o build/priority_queue.o build/main.o

build/main.o: main.c | buildDir
	$(CC) -c $< -o $@ $(CFLAGS)

build/minheap.o: minheap/minheap.c | buildDir
	$(CC) -c $< -o $@ $(CFLAGS)

build/priority_queue.o: priority_queue/priority_queue.c | buildDir
	$(CC) -c $< -o $@ $(CFLAGS)

buildDir:
	mkdir -p build

outDir:
	mkdir -p out