#include <stdlib.h>

#include "minheap.h"

MinHeapNode *new_min_heap_node(void)
{
  MinHeapNode *_new = malloc(sizeof(MinHeapNode));
  _new->value = 0;
  _new->data = NULL;
  return _new;
}

void free_min_heap_node(MinHeapNode *heap_node)
{
  free(heap_node);
}

unsigned int get_heap_parent(unsigned int current)
{
  return current / 2;
}
unsigned int get_heap_left_child(unsigned int current)
{
  return current * 2;
}
unsigned int get_heap_right_child(unsigned int current)
{
  return current * 2 + 1;
}

MinHeap *new_min_heap(void)
{
  MinHeap *_new = malloc(sizeof(MinHeap));
  if (_new != NULL)
  {
    _new->array = malloc(sizeof(MinHeapNode *) * INITIAL_HEAP_SIZE);
    _new->allocated_size = INITIAL_HEAP_SIZE;
    _new->filled_size = 0;
    for (unsigned int i = 0; i < INITIAL_HEAP_SIZE; i++)
    {
      _new->array[i] = NULL;
    }
  }
  return _new;
}

void free_min_heap(MinHeap *heap)
{
  // 0th element is always NULL
  for (unsigned int i = 1; i <= heap->filled_size; i++)
  {
    free_min_heap_node(heap->array[i]);
  }
  free(heap->array);
  free(heap);
}

int expand_min_heap(MinHeap *heap)
{
  MinHeapNode **tmp = heap->array;
  heap->array = realloc(heap->array, heap->allocated_size * HEAP_SIZE_INCREMENT);
  if (heap->array == NULL)
  {
    // Restore the original data
    heap->array = tmp;
    return EXIT_FAILURE;
  }
  heap->allocated_size *= HEAP_SIZE_INCREMENT;
  return EXIT_SUCCESS;
}

void swap_min_heap_nodes(MinHeapNode *n1, MinHeapNode *n2)
{
  // Utility function to swap pointers of MinHeadNodes
  MinHeapNode t = *n1;
  *n1 = *n2;
  *n2 = t;
}

void heapify_min_heap_rec(MinHeap *heap, unsigned int n)
{
  unsigned int left = get_heap_left_child(n);
  unsigned int right = get_heap_right_child(n);

  unsigned int smallest = n;

  if (left <= heap->filled_size && heap->array[left]->value < heap->array[n]->value)
  {
    smallest = left;
  }
  if (right <= heap->filled_size && heap->array[right]->value < heap->array[smallest]->value)
  {
    smallest = right;
  }

  if (smallest != n)
  {
    swap_min_heap_nodes(heap->array[n], heap->array[smallest]);

    heapify_min_heap_rec(heap, smallest);
  }
}

void heapify_min_heap(MinHeap *heap)
{

  // Heapify for all nodes that are not leaves;
  // going from the bottom up.
  for (int i = heap->filled_size / 2; i >= 1; i--)
    heapify_min_heap_rec(heap, i);
}

int push_min_heap(MinHeap *heap, MinHeapNode *value)
{
  // increment heap->filled_size
  heap->filled_size++;

  // Check for allocated size
  if (heap->filled_size == heap->allocated_size)
  {
    expand_min_heap(heap);
    if (heap->filled_size == heap->allocated_size)
    {
      return EXIT_FAILURE;
    }
  }

  // Insert new node
  heap->array[heap->filled_size] = value;

  unsigned int current = heap->filled_size;
  // Check parent and swap if needed;
  // rinse and repeat until either parent smaller or root
  while (1)
  {
    unsigned int parent = get_heap_parent(current);
    if (parent == 0)
    {
      // We are at root
      return EXIT_SUCCESS;
    }
    if (heap->array[current]->value < heap->array[parent]->value)
    {
      // swap...
      swap_min_heap_nodes(heap->array[current], heap->array[parent]);
      current = parent;
      continue;
    }
    return EXIT_SUCCESS;
  }
}

MinHeapNode *pop_min_heap(MinHeap *heap)
{
  // Remove the node at array[1]
  MinHeapNode *top = heap->array[1];
  // Place the last node into the first spot
  heap->array[1] = heap->array[heap->filled_size];
  heap->array[heap->filled_size] = NULL;
  heap->filled_size--;
  // re-heapify
  heapify_min_heap_rec(heap, 1);

  return top;
}

MinHeapNode *peek_min_heap(MinHeap *heap)
{
  // Return the node at array[1]
  return heap->array[1];
}