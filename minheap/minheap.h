#ifndef CONTAINERS_MINHEAP_H
#define CONTAINERS_MINHEAP_H

#define INITIAL_HEAP_SIZE 16
#define HEAP_SIZE_INCREMENT 2

#ifdef __cplusplus
extern "C"
{
#endif

  typedef struct minHeapNode
  {
    unsigned int value;
    void *data;
  } MinHeapNode;
  typedef struct minHeap
  {
    MinHeapNode **array;
    unsigned int allocated_size;
    unsigned int filled_size;
  } MinHeap;

  unsigned int get_heap_parent(unsigned int current);
  unsigned int get_heap_left_child(unsigned int current);
  unsigned int get_heap_right_child(unsigned int current);

  MinHeapNode *new_min_heap_node(void);
  void free_min_heap_node(MinHeapNode *heap_node);

  MinHeap *new_min_heap(void);
  void free_min_heap(MinHeap *heap);
  int expand_min_heap(MinHeap *heap);
  void heapify_min_heap(MinHeap *heap);

  int push_min_heap(MinHeap *heap, MinHeapNode *value);
  MinHeapNode *pop_min_heap(MinHeap *heap);
  MinHeapNode *peek_min_heap(MinHeap *heap);

#ifdef __cplusplus
}
#endif

#endif
