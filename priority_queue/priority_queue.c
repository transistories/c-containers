#include <stdlib.h>

#include "priority_queue.h"

PQ *new_p_queue(void)
{
  PQ *_new = malloc(sizeof(PQ));
  _new->heap = new_min_heap();
  return _new;
}

void free_p_queue(PQ *queue)
{
  free_min_heap(queue->heap);
  free(queue);
}

int enqueue(PQ *queue, int value, void *data)
{
  MinHeapNode *_node = new_min_heap_node();
  _node->value = value;
  _node->data = data;
  return push_min_heap(queue->heap, _node);
}
void *dequeue(PQ *queue)
{
  if (queue->heap->filled_size == 0)
    return NULL;

  MinHeapNode *_node = pop_min_heap(queue->heap);
  void *data = _node->data;
  free_min_heap_node(_node);
  return data;
}
void *peek(PQ *queue)
{
  return peek_min_heap(queue->heap)->data;
}
