#ifndef CONTAINERS_PRIORITY_QUEUE_H
#define CONTAINERS_PRIORITY_QUEUE_H

#include "minheap/minheap.h"

#ifdef __cplusplus
extern "C"
{
#endif

  typedef struct pq
  {
    MinHeap *heap;
  } PQ;

  PQ *new_p_queue(void);
  void free_p_queue(PQ *queue);

  int enqueue(PQ *queue, int value, void *data);
  void *dequeue(PQ *queue);
  void *peek(PQ *queue);

#ifdef __cplusplus
}
#endif

#endif
